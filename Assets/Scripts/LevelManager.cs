using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
	private GameObject[] mPlatforms;
	private GameObject[] mWalls;
	public CharacterController2D Character;
	private int mPlatformIndex;
	private int mWallIndex;
	public float GapSize;
	public float WallGap;
	private int mPlatformHeight;
	private int mWallWidth;
	public int DistanceToGenerateWin;
	public GameObject Background;
	public GameObject Sky;
	public GameObject FinishSpotPrefab;
	private bool mFirstTime;
	public GameObject[] coinPrefabs;
	public GameObject coinContainer;
	public GameObject platformContainer;
	public GameObject wallContainer;
	public GameObject enemmyContainer;

	public PlatformController.PlatformType Stage;
	public WallController.WallType VertStage;

	public GameObject EnemyPrefab;

	// Use this for initialization
	void Start () {
		mPlayerLastCheckPoint = 0;
		levelEnding = false;
		mFirstTime = true;
		mPlatformIndex = 0;
		mWallIndex = 0;
		mPlatforms = new GameObject[10];
		mWalls = new GameObject[10];
		GapSize += PlatformController.SIZE_FACTOR;
		WallGap += WallController.SIZE_FACTOR;
		mPlatformHeight = 0;
		mWallWidth = 0;
		mDistanceCounter = 0;
		StartCoroutine (GenerateFirstPlatforms ());
	}
	
	private int mPlayerLastCheckPoint;
	private bool levelEnding;
	// Update is called once per frame
	void Update () {

		if (Character.checkpoint > 9) {
			if(levelEnding)
				return;
			if (Character.checkpoint > mPlayerLastCheckPoint) {
				mPlayerLastCheckPoint = Character.checkpoint;
				if (Character.distance >= DistanceToGenerateWin){
					//addPlatform (2);
					levelEnding = true;
				}
				else{
					//addPlatform ();

				}
			}
		}
	}

	private bool mFirstPlayformsCreated;
	private bool mFirstWallsCreated;
	private int mDistanceCounter;


	public IEnumerator GenerateFirstPlatforms(){
		mFirstPlayformsCreated = false;
		while (!mFirstPlayformsCreated) {
			if (mFirstTime) {
				//addPlatform(1);
				//addWall();
				mFirstTime = false;
			}
			else{
				//addPlatform();
				//addWall();
			}
			yield return new WaitForSeconds(0.4f);
		}
	}

	public void addPlatform(int pPlatformOrderType = 0){
		if (mPlatforms [mPlatformIndex] != null){
			if(!mFirstPlayformsCreated){
				mFirstPlayformsCreated = true;
				return;
			}else{
				Destroy (mPlatforms [mPlatformIndex]);
			}
		}

		if(pPlatformOrderType > 0)
			mPlatforms [mPlatformIndex] = initPlatform(10);
		else
			mPlatforms [mPlatformIndex] = initPlatform(Random.Range(5,10));
		if(mPlatformIndex == 0){
			if(mPlatforms[mPlatforms.Length - 1] != null){
				mPlatforms [mPlatformIndex].transform.position = mPlatforms[mPlatforms.Length - 1].transform.position +
					new Vector3((mPlatforms[mPlatforms.Length - 1].GetComponent<PlatformController>().EndTile.transform.localPosition.x + GapSize), calculatePlatformY(), 0);
			}
		}
		else{
			mPlatforms [mPlatformIndex].transform.position = mPlatforms[mPlatformIndex - 1].transform.position +
				new Vector3((mPlatforms[mPlatformIndex - 1].GetComponent<PlatformController>().EndTile.transform.localPosition.x + GapSize), calculatePlatformY(), 0);
		}

		if (pPlatformOrderType > 0) {
			if (pPlatformOrderType == 2){
				generateFinishSpot ();
				generateEnemies ();
			}
		} else {
			generateEnemies ();
		}

		generateCoins();
		mPlatforms [mPlatformIndex].transform.SetParent(platformContainer.transform);

		mPlatformIndex++;
		if (mPlatformIndex >= mPlatforms.Length)
			mPlatformIndex = 0;

	}

	public void addWall(int pWallOrderType = 0){

		if(mWalls[mWallIndex] != null){
			if(!mFirstWallsCreated){
				mFirstWallsCreated = true;
				return;
			}else{
				Destroy(mWalls [mWallIndex]);
			}
		}

		if(pWallOrderType > 0){
			mWalls[mWallIndex] = initWall(10);
		}else{
			mWalls[mWallIndex]= initWall(Random.Range(5,10));
		}

		if(mWallIndex == 0){
			if(mWalls[mWalls.Length - 1] != null){
				mWalls [mWallIndex].transform.position = mWalls[mWalls.Length - 1].transform.position +
					new Vector3(50+calculateWallX(), (mWalls[mWalls.Length - 1].GetComponent<WallController>().EndTile.transform.localPosition.y + WallGap), 0);
			}
		}
		else{
			mWalls [mWallIndex].transform.position = mWalls[mWallIndex - 1].transform.position +
				new Vector3((mWalls[mWalls.Length - 1].GetComponent<WallController>().EndTile.transform.localPosition.y + WallGap),calculateWallX(), 0);
		}

		mWalls [mWallIndex].transform.SetParent(wallContainer.transform);

		mWallIndex++;
		if (mWallIndex >= mWalls.Length)
			mWallIndex = 0;
	}

	private void generateCoins(){
		PlatformController p = mPlatforms[mPlatformIndex].GetComponent<PlatformController>();

		foreach(GameObject g in p.MidTiles){
			if(g != null){
				GameObject coin;
				if(Random.value < 0.05f){
					coin = 
						Instantiate(coinPrefabs[1], g.transform.position +
							       	new Vector3(0, 2.0f, 0), Quaternion.Euler(0,0,0))
									as GameObject;
					}
				else{
					coin = 
						Instantiate(coinPrefabs[0], g.transform.position +
							        new Vector3(0, 0.5f, 0), Quaternion.Euler(0,0,0))
									as GameObject;
					}
				coin.transform.SetParent(coinContainer.transform);
			}
		}
	}

	private float calculatePlatformY(){
		float y = 0;

		int type = Random.Range (1, 4);

		if (type == 1){
			if (mPlatformHeight < 3){
				y = y + 1;
				mPlatformHeight++;
			}
		}
		else if (type == 2){
			if (mPlatformHeight > -1){
				y = y - 1;
				mPlatformHeight--;
			}
		}

		return y;
	}

	private float calculateWallX(){
		float x = 0;

		int type = Random.Range (1, 4);

		if (type == 1){
			if (mWallWidth < 3){
				x = x + 1;
				mWallWidth++;
			}
		}
		else if (type == 2){
			if (mWallWidth > -1){
				x = x - 1;
				mWallWidth--;
			}
		}

		return x;
	}

	private void generateEnemies(){
		GameObject e = Instantiate(EnemyPrefab,
		            		mPlatforms [mPlatformIndex].GetComponent<PlatformController>().MidTiles[Mathf.RoundToInt(( mPlatforms [mPlatformIndex].GetComponent<PlatformController>().getSize()+1)/2)].transform.position + new Vector3(-0.21f, 1, 0),
		            		Quaternion.Euler(0, -180, 0)) as GameObject;
		e.GetComponent<EnemyBehaviour>().maxPath = (mPlatforms[mPlatformIndex].GetComponent<PlatformController>().getSize())/4;
		e.transform.SetParent(enemmyContainer.transform);
	}

	private void generateFinishSpot(){
		GameObject e = Instantiate(FinishSpotPrefab,
		                           mPlatforms [mPlatformIndex].GetComponent<PlatformController>().EndTile.transform.position + new Vector3(-0.21f, 1, 0),
		                           Quaternion.Euler(0, 0, 0)) as GameObject;
	}

	public GameObject initPlatform(int size){
		GameObject container = Instantiate(Resources.Load("Platform" + getPlatformTypeName())) as GameObject;
		container.GetComponent<PlatformController> ().setSize(size);
		return container;
	}

	public GameObject initWall(int size){
		GameObject container = Instantiate(Resources.Load("Wall" + getWallTypeName())) as GameObject;
		container.GetComponent<WallController>().setSize(100);
		return container;
	}

	private string getPlatformTypeName(){
		switch (Stage) {
			case PlatformController.PlatformType.DIRT :
				return "";
			break;
			case PlatformController.PlatformType.ICE :
			return "Ice";
				break;
			case PlatformController.PlatformType.DESERT :
				return "Desert";
			break;
			case PlatformController.PlatformType.HELL :
				return "Hell";
			break;
			default :
				return "";
			break;
		}
	}

	private string getWallTypeName(){
		switch (VertStage) {
			case WallController.WallType.DIRT :
				return "";
			break;
			case WallController.WallType.ICE :
			return "Ice";
				break;
			case WallController.WallType.DESERT :
				return "Desert";
			break;
			case WallController.WallType.HELL :
				return "Hell";
			break;
			default :
				return "";
			break;
		}
	}
}
