using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {

	public GameObject BegTile;
	public GameObject[] MidTiles;
	public GameObject EndTile;
	public int PlatformId;
	public WallType WallTp;
	//public PlatformGoal PlatformGl;

	public int MaxSize;

	public const float SIZE_FACTOR = 0.42f;

	private int mSize;

	// Use this for initialization
	void Start () {
		mSize = 1;
	}

	public enum WallType{
		DIRT,
		ICE,
		DESERT,
		HELL
	}

	private string getWallTypeName(){
		switch (WallTp) {
			case WallType.DIRT :
				return "";
			break;
				case WallType.ICE :
			return "Ice";
				break;
			case WallType.DESERT :
				return "Desert";
			break;
			case WallType.HELL :
				return "Hell";
			break;
			default :
				return "";
			break;
		}
	}

	public void setSize(int size){

		for (int i = 1; i < size; i++) {
			if(MidTiles[i] == null)
				MidTiles[i] = Instantiate(Resources.Load("MIDWALL" + getWallTypeName())) as GameObject;
			MidTiles[i].transform.position = MidTiles[i-1].transform.position + new Vector3(0, SIZE_FACTOR, 0);
			MidTiles[i].transform.parent = this.transform;
			EndTile.transform.position = MidTiles[i].transform.position + new Vector3(0, SIZE_FACTOR, 0);
			this.GetComponent<BoxCollider2D>().size += new Vector2(0,0.42f);
			this.GetComponent<BoxCollider2D>().offset += new Vector2(0,0.21f);
			mSize++;
		}
	}

	public int getSize(){
		return mSize;
	}
}